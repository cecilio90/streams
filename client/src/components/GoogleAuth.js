import React, { Component } from 'react'
import { connect } from 'react-redux'

import { signIn, signOut } from '../actions'

class GoogleAuth extends Component {

    componentDidMount() {
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init({
                clientId: '1016392734160-i8qdhnajjv19h5oits0g3h74jkkmtg44.apps.googleusercontent.com',
                scope: 'email'
            }).then(() => {
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthUser(this.auth.isSignedIn.get());
                //Hay otra function aparte de get dentro de __proto__ que es listen
                //esta function recive un callback donde podemos cambiar el estado de la sesión dinámicamente
                this.auth.isSignedIn.listen(this.onAuthUser)
            })
        })
    }

    onAuthUser = (isSignedIn) => {
        if(isSignedIn) {
            this.props.signIn(this.auth.currentUser.get().getId());
        } else {
            this.props.signOut();
        }
    }

    onSignInClick = () =>{
        this.auth.signIn()
    }

    onSignOutClick = () =>{
        this.auth.signOut()
    }

    renderGoogleAuthButton() {
        if(this.props.isSignedIn === null){
            return null
        } else if (this.props.isSignedIn) {
            return (
                <button onClick={this.onSignOutClick} className="ui red google button" >
                    <i className="google icon"/>
                    Sign Out
                </button>
            )
        } else {
            return (
                <button onClick={this.onSignInClick} className="ui red google button" >
                    <i className="google icon"/>
                    Sign In with Google
                </button>
            )
        }
    }
    
    render() {
        return (
            <div>
                {this.renderGoogleAuthButton()}
            </div>
        )
    }
};

const mapStateToProps = (globalState) => {
    return {
        isSignedIn: globalState.auth.isSignedIn
    }
}

export default connect(mapStateToProps, {
    signIn,
    signOut
})(GoogleAuth);